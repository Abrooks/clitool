<?php

require dirname(__DIR__) . '/vendor/autoload.php';

// testcommand
//  -> test
//    -> default
//    -> one
//    -> two

\Cli\Command::init(
    'testcommand',
    false,
    'This is just a test command nothing to worry about'
)->addSubCommands([
    \Cli\Command::init('test', function() { \Cli\Output::output('Hit'); })->addSubCommands([
        \Cli\Command::init('one', function() { \Cli\Output::output('one'); })->addSubCommands([
            \Cli\Command::init('two', function($arguments) { \Cli\Output::output('two'); })->addArguments([
                \Cli\Argument::init('site', '--site', '-s', 'This is a site argument')->setRequired(),
                \Cli\Argument::init('ne', '--ne', '-n', 'Sure why not')->setRequired(),
            ])
        ]),
        \Cli\Command::init('testaaaa', function(){})->addArguments([
            \Cli\Argument::init('test', '--test', '-s')->setArray()
        ])
    ])
])->run();
