#!/usr/bin/env bash
_testcommand_complete () {
  WORDS=$(printf "%s " "${COMP_WORDS[@]}")
  COMPLETE=$(testcommand autocomplete "$WORDS" $COMP_CWORD)

  if [ -z "$COMPLETE" ]
  then
    COMPREPLY=()
  else
    COMPREPLY=($(compgen -W "$COMPLETE"))
  fi
}
complete -F _testcommand_complete testcommand
