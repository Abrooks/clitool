<?php

namespace Cli;

class Output
{
    public static function output($message, $colour = Colours::DEFAULT_COLOUR)
    {
        echo $colour . $message . Colours::DEFAULT_COLOUR . PHP_EOL;
    }

    public static function error($message)
    {
        self::output($message, Colours::RED);
    }
}
