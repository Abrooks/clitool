<?php

namespace Cli;

class Argument
{
    public $name;
    public $longhand;
    public $shorthand;
    public $command;
    public $required = false;
    public $boolean = false;
    public $array = false;

    public static function init($name, $longhand, $shorthand = '', $description = '')
    {
        $self = new self;
        $self->name = $name;
        $self->longhand = $longhand;
        $self->shorthand = $shorthand;
        $self->description = $description;

        return $self;
    }

    public function setRequired()
    {
        $this->required = true;

        return $this;
    }

    public function setArray()
    {
        $this->array = true;

        return $this;
    }

    public function setBoolean()
    {
        $this->boolean = true;

        return $this;
    }

    public function isValid()
    {
        if ($this->required && $this->getValue()) {
            return true;
        } elseif (!$this->required) {
            return true;
        }

        return false;
    }

    public function getValue()
    {
        $value = [];
        foreach ($this->command->argv as $i => $argument) {
            if (isset($skipNext) && $skipNext) {
                $skipNext = false;
                continue;
            }
            if ($this->boolean) {
                if (strpos($argument, $this->shorthand) !== false || strpos($argument, $this->longhand) !== false) {
                    $value[] = true;
                } else {
                    $value[] = false;
                }
            } else {
                if (isset($this->command->argv[$i + 1]) && (strpos($argument, $this->shorthand) !== false || strpos($argument, $this->longhand) !== false)) {
                    $value[] = $this->command->argv[$i + 1];
                    $skipNext = true;
                }
            }
        }

        if ($value && $this->array) {
            return $value;
        } elseif ($this->boolean && in_array(1, $value)) {
            return true;
        } elseif ($value) {
            return reset($value);
        }

        return false;
    }
}
