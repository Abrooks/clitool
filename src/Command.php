<?php

namespace Cli;

use Cli\Exceptions\ArgumentInvalid;

class Command
{
    protected $command;
    protected $callable;
    public $argv;
    protected $arguments = [];
    protected $subCommands = [];
    protected $description = '';
    public $values = [];

    public static function init($command, $callable, $description = '')
    {
        global $argv;
        $self = new self;
        $self->command = $command;
        $self->callable = $callable;
        $self->argv = $argv;
        $self->description = $description;

        return $self;
    }

    public function addSubCommands($commands)
    {
        foreach ($commands as $command) {
            $this->subCommands[$command->command] = $command;
        }

        return $this;
    }

    public function matchCommand(&$matched, $subCommand = false)
    {
        $actualCommand = false;
        $originalMatched = $matched;
        foreach ($this->subCommands as $command => $subCommand) {
            foreach ($this->argv as $key => $arg) {
                if ($arg == $command) {
                    $matched .= $command . ' ';
                    break 2;
                }
            }
        }
        if ($originalMatched !== $matched && ($subCommand && $subCommand->subCommands)) {
            $actualCommand = $subCommand->matchCommand($matched, $subCommand);
        }

        if ($originalMatched !== $matched && !$actualCommand) {
            return $subCommand;
        }

        if ($actualCommand) {
            return $actualCommand;
        }

        return false;
    }

    public function canRun($checkArguments = true)
    {
        $matched = '';
        foreach ($this->argv as $key => $arg) {
            if ($arg == $this->command || $key == 0 && strpos($arg, $this->command) !== false) {
                $matched .= $this->command . ' ';
                break;
            }
        }

        $subCommand = $this->matchCommand($matched);

        if ($this->command !== trim($matched) && ($this->subCommands && strpos(trim($matched), ' ') == false)) {
            return false;
        } elseif ($subCommand) {
            $subCommand->run(true);
        }

        if ($checkArguments) {
            foreach ($this->arguments as $argument) {
                if (!$argument->isValid()) {
                    Output::error('Argument ' . $argument->name . ' is not valid');
                    exit();
                }
            }
        }

        return true;
    }

    public function addArguments($arguments)
    {
        foreach ($arguments as $argument) {
            $argument->command = $this;
            $this->arguments[] = $argument;
        }

        return $this;
    }

    public function getArgumentValues()
    {
        if (!$this->values) {
            $values = [];

            foreach ($this->arguments as $argument) {
                $values[$argument->name] = $argument->getValue();
            }

            $this->values = $values;
        }

        return $this->values;
    }

    public function listSubCommands($padd = '    ') {
        if ($this->subCommands) {
            Output::output('');
            Output::output('Available commands:');
            foreach ($this->subCommands as $command => $_) {
                $start = str_pad($command, 20, ' ');
                Output::output($padd . $start . $_->description);
                //$_->outputArguments($padd . '    ');
            }
        }
    }

    public function outputArguments($padd = '    ') {
        if ($this->arguments) {
            Output::output('');
            Output::output('Options:');
            foreach ($this->arguments as $argument) {
                $start = str_pad($argument->longhand . ', ' . $argument->shorthand, 20, ' ');
                Output::output('  ' . $start . $argument->description);
            }
        }
    }

    public function help() {
        Output::output('Usage:');
        Output::output('  ' . $this->command . ' [arguments]');
        $this->outputArguments();

        $this->listSubCommands('  ');
        exit();
    }

    public function autocompleteMatches($i = 1, $previousMatch = false) {
        $commandSoFar = str_replace('"', '', $this->argv[count($this->argv) - 2]);
        $commandSoFar = explode(' ', trim($commandSoFar));
        if (isset($commandSoFar[$i])) {
            $match = $commandSoFar[$i];
        } else {
            $match = false;
        }
        $matches = [];

        foreach ($this->subCommands as $command => $subCommand) {
            if ($match && strpos($subCommand->command, $match) !== false && $match == $command && !(end($commandSoFar) == $command && (int)end($this->argv) === (count($commandSoFar) - 1))) {
                $i++;
                $subCommand->autocompleteMatches($i, $match);
            } elseif ($match && strpos($subCommand->command, $match) !== false) {
                $matches[] = $command;
            } else {
                $matches[] = $command;
            }

            if (strpos($command, end($commandSoFar)) !== false && (int)end($this->argv) === (count($commandSoFar) - 1)) {
                $matches = [$command];
                break;
            }
        }

        echo implode(' ', $matches);
        exit();
    }

    public function autocomplete() {
        $this->autocompleteMatches();
        exit();
    }

    public function run($alreadyChecked = false)
    {
        if ($this->canRun(false) && (end($this->argv) == '--help' || end($this->argv) == '-h')) {
            $this->help();
        }
        if ($this->canRun() || $alreadyChecked) {
            if (isset($this->argv[1]) && $this->argv[1] == 'autocomplete') {
                $this->autocomplete();
            }
            $callable = $this->callable;
            if ($callable) {
                $arguments = $this->getArgumentValues();
                $callable($arguments);
                exit();
            } else {
                $this->help();
            }
        }
    }
}
