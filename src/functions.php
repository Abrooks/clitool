<?php

use Cli\Command;

function addCommand(string $key, callable $function) {
    return Command::init($key, $function);
}

function addSubCommand(Command $command, string $key, callable $function) {
    $command->addSubCommands([
        Command::init($key, $function),
    ]);
}

function addSubCommands(Command $command, array $commands) {
    $commandsReal = [];

    foreach ($commands as $key => $commandPart) {
        $commandsReal[] = Command::init($key, $commandPart);
    }

    $command->addSubCommands($commandsReal);
}
